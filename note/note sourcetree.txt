xxx เมื่อแก้ไขไฟล์จนไฟล์เละแล้วอยากกลับไปเหมือนตอนที่ยังไม่ได้แก้ไขไฟล์นั้น (เหมือนพึ่ง push 
 	หรือเหมือนพึ่ง pull) ให้เข้าไป click ขวาที่ไฟล์ดังกล่าวแล้ว click discard
xxx ถ้า push แล้วมันขึ้น error ตรงสี่เหลี่ยมข้างหลัง ที่เขียนว่า "Track?" ให้เอาติ๊กออกทุกตัว
	ให้ใส่ติ๊กไว้เฉพาะสี่เหลี่ยมข้างหน้าที่เขียนว่า "Push?" และเลือก push เฉพาะ branch ตัวเอง
xxx ห้ามลบ folder .git เด็ดขาด ไม่งั้นใน sourcetree จะไม่ขึ้น project 
xxx ถ้าจะ checkout ที่ brach ไหน ให้เลี่ยงการ คลิกขวา checkout โดยตรง ให้ใช้เป็นคลิกขวา reset current branch to this commit
xxx ถ้า pull แล้วมัน show error แล้วบอกชื่อไฟล์ที่ error ให้ ไปที่ tab File Status เลือก ไฟล์ ที่ error 
	(ถ้าไม่เจอให้เปลี่ยนตรง sort by ... ไปเรื่อยๆ เช่น เปลี่ยนเป็น ingored หรือ modified หรือ ... ) discard ไฟล์ ที่ error ออกให้หมด แล้ว pull อีกที