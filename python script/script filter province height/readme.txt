- script นี้ชื่อ script filter province_high ทำหน้าที่คัดกรองเฉพาะภาพที่ความสูงของตัวอักษรจังหวัดในป้ายทะเบียนสูงเกิน 8 พิกเซล
โดยมีวิธีการดังนี้
1. เอาไฟล์.jpg ไปใส่ใน folder ชื่อ input1 (ถ้าไม่มีให้สร้างขึ้น)
2. เอาไฟล์.txt ซึ่งเป็น output ของ engine พี่หนึ่งซึ่งมีข้อมูล เช่น ตำแหน่งป้ายทะเบียน ตำแหน่งจังหวัด ไปใส่ใน folder ชื่อ output2 (ถ้าไม่มีให้สร้างขึ้น)
  (หากต้องการ test สามารถเอาข้อมูล test ได้จาก folder database_test)
3. รัน script script filter province_high.py (เปิด ด้วย idle แล้วกด F5) จะได้ folder เพิ่มมาอีก 1 folder คือ temp folder 1
4. output จะอยู่ใน folder ชื่อ temp folder 1
5. เอา output ไปทำ label ต่อ โดยใช้ script ของนินจา

