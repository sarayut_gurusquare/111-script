#after run this script, need to close showed image and it will show plotted histrogram
import cv2
import numpy as np
from matplotlib import pyplot as plt

im1 =cv2.imread('a1.png',1)

im2 = cv2.cvtColor(im1, cv2.COLOR_BGR2HSV)
#channal H is 0, S is 1, V is 2
channal = 0
#how many bar to represent histrogram (between 1-180)
bars = 180
hist = cv2.calcHist([im2],[channal],None,[bars],[0,179])
plt.plot(hist)
plt.show()
cv2.imshow('im1',im1)
cv2.waitKey(0)
cv2.destroyAllWindows()