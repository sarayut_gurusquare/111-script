#this sample will try to filter for green
import cv2
import numpy as np

img =cv2.imread('aa.png',1)
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

lower_green = np.array([8,0,0])
upper_green = np.array([22,255,255])

my_mask = cv2.inRange(hsv, lower_green, upper_green)
my_filtered = cv2.bitwise_and(img,img, mask= my_mask)

cv2.imshow('img',img)
cv2.imshow('my_mask',my_mask)
cv2.imshow('my_filtered',my_filtered)


cv2.waitKey(0)
cv2.destroyAllWindows()