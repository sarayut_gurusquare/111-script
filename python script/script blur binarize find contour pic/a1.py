import cv2
import os
import numpy as np

#read image
cwd             = os.getcwd()
pic1            = cv2.imread(os.path.join(cwd,'b1.png'),0)
##cv2.imshow('ori',pic1)

#blur image
pic2            = cv2.GaussianBlur(pic1,(5,5),0)
##cv2.imshow('blur',pic2)

#binarize image
ret3,pic3       = cv2.threshold(pic2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
cv2.imshow('binarized',pic3)

#find contour
_, cnts, hrc    = cv2.findContours(pic3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#draw contour
pic4            = np.zeros(pic3.shape,dtype=np.uint8)
pic5            = cv2.cvtColor(pic4,cv2.COLOR_GRAY2BGR)
for i in xrange(len(cnts)):

    pic5_1      = cv2.drawContours(pic4.copy(), [cnts[i]], 0, 255, -1)
    cv2.imshow('contour: '+str(i),pic5_1)
    pic5_2      = cv2.drawContours(pic5.copy(), [cnts[i]], 0, (255,255,0), 1)
    cv2.imshow('contour line: '+str(i),pic5_2)
#find max area contour
ctr_max_area    = max(cnts, key=cv2.contourArea)
pic6            = cv2.drawContours(pic5.copy(), [ctr_max_area], 0, (255,0,255), 1)
cv2.imshow('max area contour',pic6)


















cv2.waitKey(0)
cv2.destroyAllWindows()

