- script นี้ชื่อ script filter vote ทำหน้าที่คัดกรองเฉพาะภาพที่มีแนวโน้มจะมีป้ายทะเบียนรถยนต์
เพื่อเอาไปใช้ label จังหวัด
โดยมีวิธีการดังนี้
1. เอาไฟล์.jpg ไปใส่ใน folder ชื่อ input1
2. เอาไฟล์.txt ซึ่งเป็น output ของ engine พี่หนึ่งซึ่งมีข้อมูล เช่น ตำแหน่งป้ายทะเบียน ตำแหน่งจังหวัด ไปใส่ใน folder ชื่อ output2
3. รัน script filter vote.py จะได้ folder เพิ่มมาอีก 2 folder คือ temp folder 1 กับ temp folder 2
4. output จะอยู่ใน folder ชื่อ temp folder 2
5. เอา output ไปทำ label ต่อ โดยใช้ script ของนินจา

