from shutil import rmtree
from shutil import copyfile
from shutil import move
import os
import  base64


txt_folder = 'output2'
pic_folder = 'input1'
cwd = os.getcwd()
path_txt_folder = cwd+'/'+txt_folder
path_pic_folder = cwd+'/'+pic_folder
path_temp_folder1 = cwd+'/'+'temp_folder1'
path_temp_folder2 = cwd+'/'+'temp_folder2'
######################################
dictProvince       = {}
path_province_file = cwd+'/'+'province.txt'
f = open(path_province_file,'r')
for x1 in f:
    x2=x1.split(',')
    x3=x2[0]
    x4=base64.b64encode(x3)
    dictProvince[x4]=x2[1]
######################################
if os.path.exists(path_temp_folder1):
    rmtree(path_temp_folder1)
os.makedirs(path_temp_folder1)
if os.path.exists(path_temp_folder2):
    rmtree(path_temp_folder2)
os.makedirs(path_temp_folder2)
######################################
#filter province name existance, province name size,
my_filter = 0
txts = os.listdir(path_txt_folder)
for i in range(len(txts)):
    t1= txts[i]
    file_size = os.stat(os.path.join(path_txt_folder,t1)).st_size
    if(file_size==0):
        continue
    f = open(os.path.join(path_txt_folder,t1),'r')
    read = f.read()
    p1=read.find('province=')
    if(p1==-1):
        continue
    province_list=[]
    p2=read.find('loc=',p1)
    p3=read.find('>',p2)
    p4=p1+len('province=')
    p5=read.find(',',p4)
    xx=read[p4:p4+(p5-p4)]
    province_list.append(dictProvince[base64.b64encode(xx)])
    province_location=read[p2+4:p3].split(',')
    province_height  =int(province_location[3])-int(province_location[1])
    if(province_height<8):
        continue
    temp_pos=p5
    for j in range(4):
        p5=read.find('|',temp_pos)
        p6=read.find(',',p5)
        yy=read[p5+1:p6]
        province_list.append(dictProvince[base64.b64encode(yy)])
        temp_pos=p6
    t2=t1[:len(t1)-4]
    t_pic_old=t2+'.jpg'
    t2=t2+'--'
    for j in range(5):
        t2=t2+'_'+province_list[j]
    t_pic_new=t2+'.jpg'
    if len(t_pic_new.split('_'))!=9:
        continue
    if os.path.exists(os.path.join(path_pic_folder,t_pic_old)): 
        copyfile(os.path.join(path_pic_folder,t_pic_old),os.path.join(path_temp_folder1,t_pic_new))
        my_filter+=1
        print 'my_filter='+str(my_filter)
######################################
#vote
my_vote = 0
n = os.listdir(path_temp_folder1)
list3=[]
for i in range(len(n)):
    n1= n[i] 
    n2= n1.split('_')
    n3=n2[0]+n2[1]+n2[2]
    list3.append(n3)
group_list=[]
for i in range(len(n)):
    if i == 0:
        group_list.append(1)
    else:
        if list3[i]==list3[i-1]:
            group_list.append(group_list[i-1])
        else:
            group_list.append(group_list[i-1]+1)
group_number=max(group_list)
province_listx=[]
for i in range(group_number):
    province_listx.append([])
for i in range(len(n)):
    province_listx[group_list[i]-1].append(n[i].split('_')[4])
province_max_list=[]
for i in range(group_number):
    province_max_list.append(max(province_listx[i],key=province_listx[i].count))
match_list=[]
for i in range(group_number):
    for j in range(len(province_listx[i])):
        if province_listx[i][j]==province_max_list[i]:
            match_list.append('T')
        else:
            match_list.append('F')
final_name=[]
for i in range(len(n)):
    temp1=n[i]
    temp2=temp1[:len(temp1)-4]+'_'+match_list[i]+'.jpg'
    final_name.append(temp2)
final_folder=[]
for i in range(len(n)):
    final_folder.append(province_max_list[group_list[i]-1])
for i in range(len(n)):
    path_new_province=os.path.join(path_temp_folder2,final_folder[i])
    if not os.path.exists(path_new_province):
        os.makedirs(path_new_province)
    if os.path.exists(os.path.join(path_temp_folder1,n[i])):
        #print 'xxxx_'+os.path.join(path_temp_folder1,n[i])
        xxx=final_name[i]
        yyy=len(xxx)
        if xxx[yyy-5:yyy-4]=='T':
            copyfile(os.path.join(path_temp_folder1,n[i]),os.path.join(path_new_province,final_name[i]))
            my_vote+=1
            print 'my_vote='+str(my_vote)









