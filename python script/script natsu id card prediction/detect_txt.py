#!/usr/bin/env python
# binarized and remove blue component color image
# by natsu :3

import numpy as np
from scipy import misc
from scipy import ndimage
from glob import glob
from PIL import Image 
import sys
import cv2

# path input
image_color_path 	= "input/03.png"		# sys.argv[1]
image_binary_path 	= "input/03.bin.png"	# sys.argv[2]

# create blue channel image
# ch color: 0-blue, 1-green, 2-red
image_color 	= cv2.imread(image_color_path)
image_blue 		= image_color
image_blue[:,:,1] = image_blue[:,:,0]
image_blue[:,:,2] = image_blue[:,:,0]
#cv2.imwrite('blue.jpg', image_blue)

# create binary image
# value: 0-black, 1-white
image_binary = misc.imread(image_binary_path, 1)/255.0
image_binary = image_binary-1			# invert color

# parameter
h = image_binary.shape[0]
w = image_binary.shape[1]

# find connected component from binary image
labeled, n_object = ndimage.label(image_binary)	# labeled by number / n_object: no. of components

# create blue_ch array
blue_ch = np.zeros( (n_object, 255) )

for i in range(n_object):
	# get component
	fragment 	= ndimage.find_objects(labeled)[i]
	imgfrag 	= labeled[fragment]
	imgfraguse 	= np.zeros( (imgfrag.shape[0], imgfrag.shape[1]) )

	# count blue channel to array
	for ii in range(imgfrag.shape[0]):
		for jj in range(imgfrag.shape[1]):
			if labeled[ fragment[0].start+ii ][ fragment[1].start+jj ] == i+1 :
				blue_ch[i][ image_blue[fragment[0].start+ii][fragment[1].start+jj] ] +=1

# find average tone of grayscale
avg_tone 	= np.zeros(n_object)
for i in range(n_object):
	sum_loop = 0
	cnt_pixel = 0
	for j in range(255):
		sum_loop+=(j * blue_ch[i][j])
		cnt_pixel+=blue_ch[i][j]
	avg_tone[i] 	= int(sum_loop/cnt_pixel)

#print "max: ", np.amax(avg_tone)
#print "min: ", np.amin(avg_tone)
print "max - min:", int((np.amax(avg_tone) + np.amin(avg_tone))/2)

threshold = int((np.amax(avg_tone) + np.amin(avg_tone))/2)

# delete component if avg_tone > threshold (too white)
for i in range(n_object):
	# get component
	fragment 	= ndimage.find_objects(labeled)[i]
	imgfrag 	= labeled[fragment]
	imgfraguse 	= np.zeros( (imgfrag.shape[0], imgfrag.shape[1]) )

	# count blue channel to array
	for ii in range(imgfrag.shape[0]):
		for jj in range(imgfrag.shape[1]):
			#if labeled[ fragment[0].start+ii ][ fragment[1].start+jj ] == i+1 and :
			if avg_tone[i] > threshold:
				image_binary[fragment[0].start+ii][fragment[1].start+jj] = 0

misc.imsave("out1.png", image_binary)

# find connected component from image_binary
img = image_binary

labeled, n_object = ndimage.label(img)
imgmark = np.zeros( (h, w) )

for i in range(0, n_object):
	# get component
	fragment = ndimage.find_objects(labeled)[i]
	imgfrag = labeled[fragment]
	imgfraguse = np.zeros( (imgfrag.shape[0], imgfrag.shape[1]) )

	ii_start = int(fragment[0].start - ( 0.2 * imgfrag.shape[0]))		# up
	ii_stop  = int(fragment[0].stop  + ( 0.2 * imgfrag.shape[0]))		# down
	jj_start = int(fragment[1].start - ( 0.15 * imgfrag.shape[1]))		# left
	jj_stop  = int(fragment[1].stop  + ( 0.15 * imgfrag.shape[1]))		# right
	
	for ii in range(ii_start, ii_stop):
		for jj in range(jj_start, jj_stop):
			imgmark[ii][jj] = 1

misc.imsave("out3.png", imgmark)

# delete touch border components
label_num, num = ndimage.measurements.label(imgmark)
mark_label = np.zeros(num+1)
		
# from border
# left top to left bottom
for i in range(0, h):
	if imgmark[i][0] == 1:
		mark_label[label_num[i][0]] = 1
# right top to right bottom
for i in range(0, h):
	if imgmark[i][w-1] == 1:
		mark_label[label_num[i][w-1]] = 1
# left top to right top
for j in range(0, w):
	if imgmark[0][j] == 1:
		mark_label[label_num[0][j]] = 1
# left bottom to right bottom
for j in range(0, w):
	if imgmark[h-1][j] == 1 :
		mark_label[label_num[h-1][j]] = 1

for i in range(0, h):
	for j in range(0, w):
		if imgmark[i][j]==1 and mark_label[label_num[i][j]]==1  :
			imgmark[i][j] = 0

# delete small components ( component width < 5, height < 5 )
labeled_mark, n_object_mark = ndimage.label(imgmark)

for i in range(0, n_object_mark):
	# get component
	fragment = ndimage.find_objects(labeled_mark)[i]
	imgfrag = labeled_mark[fragment]
	if imgfrag.shape[0] < 5 or imgfrag.shape[1] < 5:
		ii_start = fragment[0].start
		ii_stop  = fragment[0].stop  
		jj_start = fragment[1].start 
		jj_stop  = fragment[1].stop
		for ii in range(ii_start, ii_stop):
			for jj in range(jj_start, jj_stop):
				imgmark[ii][jj] = 0

# get only mark white 1
image_output = np.zeros( (h, w) )
# use mark to binary images
for i in range(0, h):
	for j in range(0, w):
		if imgmark[i][j] == 1:
			image_output[i][j] = image_binary[i][j]

# finally, cleanpad
imgin = np.zeros( (h,w) )

for i in range(0, h):
	for j in range(0, w):
		if image_output[i][j] == 0:
			imgin[i][j] = 1

padw = 1-np.zeros((10,w))
padh = 1-np.zeros((h+20,10))

imgin = np.append(padw, imgin, axis=0)
imgin = np.append(imgin, padw, axis=0)
imgin = np.append(padh, imgin, axis=1)
imgin = np.append(imgin, padh, axis=1)

w = w+20
h = h+20

beg_w = 0
end_w = 0
beg_h = 0
end_h = 0

i=0
while (i<w):
	for j in range(0, h):
		if( imgin[j][i]==0 ):
			beg_w = i
			break
	if(beg_w > 0): break
	i+=1

i=w-1
while (i>0):
	for j in range(0, h):
		if( imgin[j][i]==0 ):
			end_w = i
			break
	if(end_w > 0): break
	i-=1

i=0
while (i<h):
	for j in range(0, w):
		if( imgin[i][j]==0 ):
			beg_h = i
			break
	if(beg_h > 0): break
	i+=1

i=h-1
while (i>0):
	for j in range(0, w):
		if( imgin[i][j]==0 ):
			end_h = i
			break
	if(end_h > 0): break
	i-=1

imgout = imgin[beg_h-3:end_h+3 , beg_w-3:end_w+3]

misc.imsave("out.png", imgout)




