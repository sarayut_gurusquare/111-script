import numpy as np
import cv2
import datetime
import sys
import os

def f_video_2_pic(source_path,destination_path):
    filename=os.path.basename(source_path)
    filename_without_format= filename[:-4]

    cap = cv2.VideoCapture(source_path)
    ret = True
    count=0
    while(cap.isOpened() and ret):
        ret, frame = cap.read()
        if(ret):
            count=count+1
            a=datetime.datetime.now()
            b=a.strftime('%Y_%m_%d_%H_%M_%S')+'_'+str(a.microsecond)
            c=filename_without_format+'-'+str(count)+'-'+b+'.png'
            
            cv2.imwrite(os.path.join(destination_path,c),frame)
        ##quit display by press 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
   
source_path=sys.argv[1]
destination_path=sys.argv[2]
##source_path=r"D:\project YIP\script python video to pic\drop.avi"
##destination_path=r"D:\project YIP\script python video to pic"
f_video_2_pic(source_path,destination_path)
                            




##filename = 'drop.avi'
##filename_without_format= filename[:-4]

##cap = cv2.VideoCapture(filename)
##ret = True
##count=0
##while(cap.isOpened() and ret):
##    ret, frame = cap.read()
##    if(ret):
##        count=count+1
##        a=datetime.datetime.now()
##        b=a.strftime('%Y_%m_%d_%H_%M_%S')+'_'+str(a.microsecond)
##        cv2.imwrite(filename_without_format+'-'+str(count)+'-'+b+'.jpg',frame)
##    ##quit display by press 'q'
##    if cv2.waitKey(1) & 0xFF == ord('q'):
##        break
##
##cap.release()
##cv2.destroyAllWindows()
