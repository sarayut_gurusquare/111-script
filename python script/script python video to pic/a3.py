import numpy as np
import cv2
import datetime

filename = 'drop.avi'
filename_without_format= filename[:-4]

cap = cv2.VideoCapture(filename)
ret = True
count=0
while(cap.isOpened() and ret):
    ret, frame = cap.read()
    if(ret):
        count=count+1
        a=datetime.datetime.now()
        b=a.strftime('%Y_%m_%d_%H_%M_%S')+'_'+str(a.microsecond)
        cv2.imwrite(filename_without_format+'-'+str(count)+'-'+b+'.jpg',frame)
    ##quit display by press 'q'
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
