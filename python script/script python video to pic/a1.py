import cv2
import os

cwd = os.getcwd()
vid_path = os.path.join(cwd,'drop.avi')

cap = cv2.VideoCapture(vid_path)
ret = True
while(cap.isOpened() and ret):
    ret, frame = cap.read()
    if(ret):
        cv2.imshow('frame',frame)
    ##quit display by press 'q'
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

