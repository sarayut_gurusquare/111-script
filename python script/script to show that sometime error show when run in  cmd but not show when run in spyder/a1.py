# run this script with command line:
# 1. open cmd and cd to working directory 
# 2. run script with the following line
#       python a1.py aaa.jpg
# 3. it will show error with the following line
#       Corrupt JPEG data: 2 extraneous bytes before marker 0xd9

import cv2, sys
def main(filepath):
    im1 = cv2.imread(filepath)
    print 'shape is='+str(im1.shape)

if __name__ == "__main__":
    main(sys.argv[1])

