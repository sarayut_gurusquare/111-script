import cv2
import os
import numpy as np

#read image
cwd             = os.getcwd()
pic1            = cv2.imread(os.path.join(cwd,'b1.png'),0)
cv2.imshow('ori',pic1)

#blur image
pic2            = cv2.GaussianBlur(pic1,(5,5),0)
cv2.imshow('blur',pic2)

#binarize image
ret3,pic3       = cv2.threshold(pic2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
cv2.imshow('binarized',pic3)

#find contour
_, cnts, hrc    = cv2.findContours(pic3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

for i in xrange(len(cnts)):
    pic4        = np.ones(pic3.shape,dtype=np.uint8)
    pic5        = cv2.drawContours(pic4, [cnts[i]], 0, 255, -1)
    cv2.imshow('contour: '+str(i),pic5)

    






















cv2.waitKey(0)
cv2.destroyAllWindows()

