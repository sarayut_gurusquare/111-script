import cv2

#old methond
##im1     = cv2.imread('char.png',1)
##im2     = cv2.cvtColor(im1,cv2.COLOR_BGR2GRAY)
##rows,cols,_ = im1.shape
##
##cv2.imshow('im1',im1)
##
##M = cv2.getRotationMatrix2D((cols/2,rows/2),180,1)
##im3 = cv2.warpAffine(im1,M,(cols,rows))
##cv2.imshow('im3',im3)




#new method
im1     = cv2.imread('char.png',1)
cv2.imshow('im1',im1)
im2     = cv2.flip(im1, -1)
cv2.imshow('im2',im2)
cv2.waitKey(0)
cv2.destroyAllWindows()
