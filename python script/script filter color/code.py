import os,cv2, numpy as np
##read image
file_name='a1.jpg'
cwd=os.getcwd()
pic_path=os.path.join(cwd,'dataset',file_name)
img_color=cv2.imread(pic_path,1)

#smooth image
img_smooth = cv2.bilateralFilter(img_color,19,85,85)

#filter only black color in idcard
img_hsv = cv2.cvtColor(img_smooth,cv2.COLOR_BGR2HSV)
lower_black = np.array([40,0,0])
upper_black = np.array([255,255,150])
img_black = cv2.inRange(img_hsv, lower_black, upper_black)

img_black_inv = cv2.bitwise_not(img_black)
cv2.imshow('b1',img_black_inv)
cv2.imwrite('b1.png',img_black_inv)












cv2.waitKey(0)
cv2.destroyAllWindows()

