# Import the cv2 library
import cv2
# Read the image you want connected components of
src = cv2.imread('/directorypath/image.bmp')
# Threshold it so it becomes binary
ret, thresh = cv2.threshold(src,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
# You need to choose 4 or 8 for connectivity type
connectivity = 4  
# Perform the operation
output = cv2.connectedComponentsWithStats(thresh, connectivity, cv2.CV_32S)
# Get the results
# The first cell is the number of labels
num_labels = output[0]
# The second cell is the label matrix
labels = output[1]
# The third cell is the stat matrix
stats = output[2]
# The fourth cell is the centroid matrix
centroids = output[3]


cv2.CC_STAT_LEFT The leftmost (x) #coordinate which is the inclusive start of the bounding box in the horizontal direction.
cv2.CC_STAT_TOP The topmost (y) #coordinate which is the inclusive start of the bounding box in the vertical direction.
cv2.CC_STAT_WIDTH #The horizontal size of the bounding box
cv2.CC_STAT_HEIGHT #The vertical size of the bounding box
cv2.CC_STAT_AREA #The total area (in pixels) of the connected component