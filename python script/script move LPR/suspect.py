from shutil import move
from shutil import rmtree
from shutil import copyfile
import os
import time
import gc
import time
#import guppy

no_file_in_folder   = 100
sleep_in_seconds    = 10*60

cwd           = os.getcwd()
src_folder    = r'C:\guru-lpr\storage\ftp-sync\lpr\pending'
target_folder = r'C:\guru-lpr\services\ftp-sync\data\lpr\bak_out'
temp_folder   = r'C:\guru-lpr\services\ftp-sync\data\lpr\temp_folder'

assert os.path.exists(src_folder)
assert os.path.exists(target_folder)
if os.path.exists(temp_folder):
   rmtree(temp_folder)
os.makedirs(temp_folder)

files               = os.listdir(src_folder)
len_files           = len(files)
del files
gc.collect()
del gc.garbage[:]
my_mod              = len_files % no_file_in_folder
no_folder           = len_files / no_file_in_folder
#write file
count = 0
for i in xrange(0,no_folder):
   first      = i*no_file_in_folder
   second     = first + no_file_in_folder
   files      = os.listdir(src_folder)
   temp_files = files[first:second]
   del files
   gc.collect()
   del gc.garbage[:]
   
   file = open(temp_folder+'/a'+str(i+1)+'.txt','w')
   for j in temp_files:
       file.write(j)
       file.write('\n')
       count += 1
       print 'w'+str(count)
   file.close()
   del file
   del temp_files
   gc.collect()
   del gc.garbage[:]
   #time.sleep(sleep_in_seconds)

if my_mod != 0 :
   files      = os.listdir(src_folder)
   temp_files = files[second:]
   del files
   gc.collect()
   del gc.garbage[:]
   
   file = open(temp_folder+'/a'+str(no_folder+1)+'.txt','w')
   for j in temp_files:
       file.write(j)
       file.write('\n')
       count += 1
       print 'w'+str(count)
   file.close()
   del file
   del temp_files
   gc.collect()
   del gc.garbage[:]
   time.sleep(sleep_in_seconds)
   
#move file
count = 0
if my_mod == 0:
   real_no_folder = no_folder
else:
   real_no_folder = no_folder + 1
   
for i in xrange(0,real_no_folder):
   file = open(temp_folder+'/a'+str(i+1)+'.txt','r')
   for line in file:
       line      = line[:len(line)-1]
       temp1     = line.split('_')
       temp2     = temp1[2:]
       new_line  = temp2[0]
       new_line  = '_'.join(temp2)
       move(src_folder + '/' + line,target_folder + '/' + new_line)
       count += 1
       print 'm'+str(count)
   file.close()
   del file
   gc.collect()
