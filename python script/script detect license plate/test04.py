import cv2
import numpy as np
from matplotlib import pyplot as plt
import os
from os import listdir
from os.path import isfile, join

mypath = os.getcwd()
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
for j in files:
    if j[-3:]=='jpg':
        #grayscale
        img1 = cv2.imread(j,0)
        ##cv2.imshow('gray',img1)

        #median filter
        img1 = cv2.medianBlur(img1,5)

        #blur
        img2 = cv2.bilateralFilter(img1,9,75,75)
        ##img2 = cv2.GaussianBlur(img1,(7,7),0)
        ##img2 = cv2.blur(img1,(5,5))
        ##cv2.imshow('blur',img2)

        #binary
        ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
        ##cv2.imshow('binary',img3)

        #find contour
        img5, contours, hierarchy = cv2.findContours(img3,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)

        #pick contours which is parent to other contour
        parent_contours=[]
        for i in range(0,len(hierarchy[0])):
            if hierarchy[0,i,2] != -1:
                parent_contours.append(contours[i])

        ##contour to convex hull
        hulls = parent_contours
        for i in range(0,len(parent_contours)):
            hulls[i] = cv2.convexHull(parent_contours[i])
        contours=hulls

        #pic to draw contour
        img6 = cv2.cvtColor(img1,cv2.COLOR_GRAY2RGB)

        #draw contour
        cv2.drawContours(img6, contours, -1, (255,0,255), 1)
        cv2.imshow(j,img6)
##################
cv2.waitKey(0)
cv2.destroyAllWindows()













