import cv2
import numpy as np
import os
import shutil

def f_pic_compare(pic_a,pic_b):
    my_sum=0
    my_size=pic_a.size
    x,y=pic_a.shape
    for i in xrange(x):
        for j in xrange(y):
            if pic_a[i,j]==pic_b[i,j]:
                my_sum=my_sum+1
    if my_sum==my_size:
        return 1
    else:
        return 0

def f_detect_letter_color(order_number_of_contour,all_contours,all_hierachies,image_binary):
    #function to detect letter in binary image
    #output = 0 -> not letter
    #output = 1 -> white letter without hole
    #output = 2 -> black letter without hole
    #output = 3 -> white letter with hole 
    #output = 4 -> black letter with hole
    area_max=700
    area_min=90
    area=cv2.contourArea(all_contours[order_number_of_contour])
    if area > area_max or area < area_min:
        return 0
    im_self=np.zeros(image_binary.shape,np.uint8)
    cv2.drawContours(im_self,[all_contours[order_number_of_contour]],0,255,-1)
    im_x=cv2.bitwise_and(image_binary,image_binary,mask=im_self)
    if f_pic_compare(im_self,im_x)==1:
        return 1
    else:
        img, contours_x, hierarchy_x = cv2.findContours(im_x,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        im_x_child=np.zeros(image_binary.shape,np.uint8)
        cv2.drawContours(im_x_child,[contours_x[1]],0,255,-1)    
        if f_pic_compare(im_x_child,im_self)==0:
            return 3
        else:
            h=hierarchy_x[0]
            if len(h)==2:
                return 2
            else:
                return 4
def f_detect_letter_bottommost(order_number_of_contour,all_contours):
    cnt=all_contours[order_number_of_contour]
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
    return bottommost[1]
################################################################################################
file_name='car4_1.jpg'

#grayscale
im_full_color=cv2.imread(file_name,1)
im_full_gray =cv2.imread(file_name,0)
h0,w0 = im_full_gray.shape

#crop only 1/2 of image below & 3/5 of middle
h1=h0*1/2
w1=w0/5
w2=w0-w1
im_crop_gray=im_full_gray[h1:h0,w1:w2]
##cv2.imshow('crop',img1)

#im_crop_color and mask_crop to draw contour
im_crop_color = cv2.cvtColor(im_crop_gray,cv2.COLOR_GRAY2RGB)
mask_crop = np.zeros(im_crop_gray.shape,np.uint8)

#median filter
im_crop_median = cv2.medianBlur(im_crop_gray,3)
##cv2.imshow('medianblur',img1x)

#blur
im_crop_blur = cv2.bilateralFilter(im_crop_median,15,35,35)
##cv2.imshow('bifil',img2)

#adaptive binary
im_crop_bi = cv2.adaptiveThreshold(im_crop_blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        cv2.THRESH_BINARY,63,2)
cv2.imshow('im_crop_bi',im_crop_bi)

img, contours_bi, hierarchy_bi = cv2.findContours(im_crop_bi,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#find white letter with or without hole and size like letter -> call x
a_candidate_letter1=[]
for i in xrange(len(contours_bi)):
    temp = f_detect_letter_color(i,contours_bi,hierarchy_bi,im_crop_bi)
    if temp == 1 or temp == 3:
        a_candidate_letter1.append(i)
        
#find x with almost the same bottommost
#create dictionary key=order of contour value = bottommost
d_bottommost={}
for i in a_candidate_letter1:
    bottommost=f_detect_letter_bottommost(i,contours_bi)
    d_bottommost[i]=bottommost

a_bottommost=[]
a_candidate_letter2=[]
for i in a_candidate_letter1:
    if len(a_bottommost)==0:
        a_bottommost.append([d_bottommost[i]])
        a_candidate_letter2.append([i])
    else:
        1=1


print a_bottommost
print a_candidate_letter2                            
        
cv2.waitKey(0)
cv2.destroyAllWindows()
##    

    








