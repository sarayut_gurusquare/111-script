import cv2
import numpy as np
from matplotlib import pyplot as plt

#grayscale
img1 = cv2.imread('car1.jpg',0)
cv2.imshow('gray',img1)

#blur
img2 = cv2.bilateralFilter(img1,9,75,75)
cv2.imshow('blur',img2)

#binary
ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
cv2.imshow('binary',img3)

#opening
kernel = np.ones((3,3),np.uint8)
img4 = cv2.morphologyEx(img3, cv2.MORPH_OPEN, kernel)
cv2.imshow('opening',img4)

#contour rectangle with big area
img5, contours, hierarchy = cv2.findContours(img4,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
img6 = cv2.cvtColor(img1,cv2.COLOR_GRAY2RGB)
for cnt in contours:
    approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)
    if len(approx)==4 and len(cnt)>70:
        cv2.drawContours(img6, [approx], 0, (0,255,0), 3)
cv2.imshow('rectangle',img6)
##################
cv2.waitKey(0)
cv2.destroyAllWindows()





##import cv2
##import numpy as np
##from matplotlib import pyplot as plt
##
###grayscale
##img1 = cv2.imread('car1.jpg',0)
####cv2.imshow('gray',img1)
##
###blur
##img2 = cv2.bilateralFilter(img1,9,75,75)
####cv2.imshow('blur',img2)
##
###binary
##ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
####cv2.imshow('binary',img3)
##
###opening
##kernel = np.ones((3,3),np.uint8)
##img4 = cv2.morphologyEx(img3, cv2.MORPH_OPEN, kernel)
##cv2.imshow('opening',img4)
## [subcode]
##import os
##from os import listdir
##from os.path import isfile, join
##
##import cv2
##import numpy as np
##from matplotlib import pyplot as plt
##
##mypath = os.getcwd()
##files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
##for i in files:
##    if i[-3:]=='jpg':
##        #grayscale
##        img1 = cv2.imread(i,0)
####        cv2.imshow('gray',img1)
##
##        #blur
##        img2 = cv2.bilateralFilter(img1,9,75,75)
####        cv2.imshow('blur',img2)
##
##        #binary
##        ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
##        cv2.imshow('binary',img3)
##
##        #opening
##        kernel = np.ones((3,3),np.uint8)
##        img4 = cv2.morphologyEx(img3, cv2.MORPH_OPEN, kernel)
####        cv2.imshow('opening',img4)
##
##        #contour
##        img5, contours, hierarchy = cv2.findContours(img4,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
##        cnt = contours[1]
##        a1,a2=img1.shape
##        img_black = np.zeros([a1,a2,3], np.uint8)
##        img6 = cv2.drawContours(img_black, contours, -1, (0,255,0), 1)
####        cv2.imshow('contour',img6)
##        ####################
##        cv2.waitKey(0)
##        cv2.destroyAllWindows()   
##
##













