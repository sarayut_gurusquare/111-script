import cv2
import numpy as np
from matplotlib import pyplot as plt
import os
import shutil
from os import listdir
from os.path import isfile, join

mypath = os.getcwd()
input1 = os.path.join(mypath,'input1')
output1 =os.path.join(mypath,'output1')
if os.path.exists(output1):
    shutil.rmtree(output1)
os.makedirs(output1)
files =listdir(input1)
count=0
for j in files:
    if j[-3:]=='jpg':
        print j
        #grayscale
        img1 = cv2.imread(j,0)
        ##cv2.imshow('gray',img1)

        #pic to draw contour
        img6 = cv2.cvtColor(img1,cv2.COLOR_GRAY2RGB)

        ##median filter
        img1 = cv2.medianBlur(img1,5)

        #blur
        img2 = cv2.bilateralFilter(img1,9,75,75)
##        img2 = cv2.GaussianBlur(img1,(7,7),0)
        ##img2 = cv2.blur(img1,(5,5))
        ##cv2.imshow('blur',img2)

        #binary
        ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
        ##cv2.imshow('binary',img3)

        #find contour
        img5, contours, hierarchy = cv2.findContours(img3,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)

        #pick contours which is parent to other contour
        parent_contours=[]
        for i in range(0,len(hierarchy[0])):
            if hierarchy[0,i,2] != -1:
                parent_contours.append(contours[i])

        #contour to convex hull
        hull_contours = parent_contours
        for i in range(0,len(parent_contours)):
            hull_contours[i] = cv2.convexHull(parent_contours[i])

##        #draw convex hull
##        cv2.drawContours(img6, hull_contours, -1, (255,0,255), 2)

        #filter by bounding box's properties
        contours=[]
        wh_r_max = 4
        wh_r_min = 2
        area_r_max = 14000
        area_r_min = 5000
        degree_max = 2.5
        degree_min = -2.5
        area_ratio_min = 0.8
        for i in range(0,len(hull_contours)):
            is_license_plate = 0
            temp = [False,False,False,False]
            cnt = hull_contours[i]
            #rotated bounding box
            rect = cv2.minAreaRect(cnt)
            wr = rect[1][0]
            hr = rect[1][1]
            area_r = wr*hr
            wh_r = wr/hr
            degree = rect[2]
            #straigt bounding box
            x,y,ws,hs = cv2.boundingRect(cnt)
            area_s = ws*hs
            area_ratio = area_r / area_s
            if wh_r_min < wh_r < wh_r_max:
                temp[0]=True
            if area_r_min < area_r < area_r_max :
                temp[1]=True
            if degree_min < degree < degree_max:
                temp[2]=True
            if area_ratio_min < area_ratio:
                temp[3]=True
            if all(x == True for x in temp):
                is_license_plate = True
            if is_license_plate == True:
                box = cv2.boxPoints(rect)
                box = np.int0(box)
                cv2.drawContours(img6,[box],0,(0,255,0),2)
        cv2.imwrite(os.path.join(output1,j),img6)
        count=count+1
        print count
##        cv2.imshow(j,img6)
####################
##cv2.waitKey(0)
##cv2.destroyAllWindows()













