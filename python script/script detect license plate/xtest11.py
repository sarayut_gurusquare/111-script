import cv2
import numpy as np
import os
import shutil

file_name='car4_1.jpg'


#grayscale
im_full_color=cv2.imread(file_name,1)
im_full_gray =cv2.imread(file_name,0)
h0,w0 = im_full_gray.shape

#crop only 1/2 of image below & 3/5 of middle
h1=h0*1/2
w1=w0/5
w2=w0-w1
im_crop_gray=im_full_gray[h1:h0,w1:w2]
##cv2.imshow('crop',img1)

#im_crop_color and mask_crop to draw contour
im_crop_color = cv2.cvtColor(im_crop_gray,cv2.COLOR_GRAY2RGB)
mask_crop = np.zeros(im_crop_gray.shape,np.uint8)

#median filter
im_crop_median = cv2.medianBlur(im_crop_gray,3)
##cv2.imshow('medianblur',img1x)

#blur
im_crop_blur = cv2.bilateralFilter(im_crop_median,15,35,35)
##cv2.imshow('bifil',img2)

#adaptive binary
im_crop_bi = cv2.adaptiveThreshold(im_crop_blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        cv2.THRESH_BINARY,63,2)
cv2.imshow('im_crop_bi',im_crop_bi)

#find contour
img, contours_crop_bi, hierarchy_crop_bi = cv2.findContours(im_crop_bi,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
for i in xrange(320):
    temp=cv2.cvtColor(im_crop_gray,cv2.COLOR_GRAY2RGB)
    cv2.drawContours(temp,[contours_crop_bi[i]],0,(0,255,0),2)
    area=cv2.contourArea(contours_crop_bi[i])
    if area>50:
        cv2.imshow(str(i)+'_'+str(area),temp)


cv2.waitKey(0)
cv2.destroyAllWindows()
## [subcode]




###find contour : second time
##img5, contours, hierarchy = cv2.findContours(img3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
##
##if len(contours)>0:
##    h=hierarchy[0]
##  
##    #create dic key=parent =array of children
##    d_children={}
##    for i in xrange(len(h)):
##        H=h[i][3]
##        if H != -1:
##            if d_children.has_key(H)== False:
##                d_children[H]=[]
##                d_children[H].append(i)
##            else:
##                d_children[H].append(i)
##    print d_children
##    #create dic key=parent =color of first children (0:black,1:white) 
##    d_color_children={}
##    a_parent=dict.keys(d_children)
##    for i in a_parent:
##        print i
##        a_child=d_children[i]
##        first_child=a_child[0]
##        print first_child
##        print 'xxxxxxxxxxxxx'
##        first_child_pic = np.zeros(img1.shape,np.uint8)
##        cv2.drawContours(first_child_pic,[contours[first_child]],0,255,-1)
##        cv2.imshow(str(first_child),first_child_pic)
##        ori=img3
##        AND=cv2.bitwise_and(ori,ori,mask=first_child_pic)
##        pos_AND=cv2.findNonZero(AND)
##        pos_first_child=cv2.findNonZero(first_child_pic)
##        if len(pos_AND) == len(pos_first_child):
##            d_color_children[i]=1
##        else:
##            d_color_children[i]=0
##    print d_color_children  
##    #create dic key=parent value=parent's size
##    d_parent_size={}
##    for i in a_parent:
##        d_parent_size[i] = cv2.contourArea(contours[i])
##    print d_parent_size
##    #criteria for soldier license plate
##    min_no_children=2
##    min_parent_size=500
##
##    has_soldier_plate=0
##    for i in a_parent:
##        cri=[0,0,0]
##        if len(d_children[i])>=min_no_children:
##            cri[0]=1
##        if d_color_children[i]==1:
##            cri[1]=1
##        if d_parent_size[i]>=min_parent_size:
##            cri[2]=1
##        if sum(cri)==len(cri):
##            cv2.drawContours(img6,[contours[i]],0,(0,255,0),1)
##            children=d_children[i]
##            for j in children:
##                cv2.drawContours(img6,[contours[j]],0,(0,0,255),1)
##            has_soldier_plate=1
##    if has_soldier_plate==1:
##        img_ori[h1:h0,w1:w2]=img6
##        cv2.imwrite('output_'+file_name,img_ori)
##        cv2.imshow('img',img_ori)     
##        cv2.waitKey(0)
##        cv2.destroyAllWindows()
##        
##
## 
##
##
##
##            
##
##
##
##
##
##
##


    
##cv2.imshow('bi',img3)
##cv2.waitKey(0)
##cv2.destroyAllWindows()




    



##

