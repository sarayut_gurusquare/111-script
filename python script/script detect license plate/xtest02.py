import cv2
import numpy as np
import os
import shutil

#input & output
cwd = os.getcwd()
input_folder_path=os.path.join(cwd,'input')
output_folder_path=os.path.join(cwd,'output')
if os.path.exists(output_folder_path):
    shutil.rmtree(output_folder_path)
os.makedirs(output_folder_path)
files =os.listdir(input_folder_path)
count=0
input_len=len(files)

for file_name in files:
    if file_name[-11:]=='process.jpg':
        continue
    
    #grayscale
    img_ori=cv2.imread(os.path.join(input_folder_path,file_name),1)
    img0=cv2.imread(os.path.join(input_folder_path,file_name),0)
    h0,w0 = img0.shape

    #crop only 1/3 of image below & 3/5 of middle
    h1=h0*2/3
    w1=w0/5
    w2=w0-w1
    img1=img0[h1:h0,w1:w2]
    ##cv2.imshow('crop',img1)

    #rgb and binary to draw contour
    img6 = cv2.cvtColor(img1,cv2.COLOR_GRAY2RGB)
    mask = np.zeros(img1.shape,np.uint8)

    #median filter
    img1x = cv2.medianBlur(img1,5)
    ##cv2.imshow('medianblur',img1x)

    #blur
    img2 = cv2.bilateralFilter(img1x,9,75,75)
    ##cv2.imshow('bifil',img2)

    #binary
    ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
    ##cv2.imshow('bi',img3)

    #find contour
    img5, contours, hierarchy = cv2.findContours(img3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    if len(contours)>0:
        h=hierarchy[0]
        
        #create dic key=parent =array of children
        d_children={}
        for i in xrange(len(h)):
            H=h[i][3]
            if H != -1:
                if d_children.has_key(H)== False:
                    d_children[H]=[]
                    d_children[H].append(i)
                else:
                    d_children[H].append(i)
                    
        #create dic key=parent =color of first children (0:black,1:white) 
        d_color_children={}
        a_parent=dict.keys(d_children)
        for i in a_parent:
            a_child=d_children[i]
            first_child=a_child[0]
            first_child_pic = np.zeros(img1.shape,np.uint8)
            cv2.drawContours(first_child_pic,[contours[first_child]],0,255,-1)
            ori=img3
            AND=cv2.bitwise_and(ori,ori,mask=first_child_pic)
            pos_AND=cv2.findNonZero(AND)
            pos_first_child=cv2.findNonZero(first_child_pic)
            if len(pos_AND) == len(pos_first_child):
                d_color_children[i]=1
            else:
                d_color_children[i]=0

        #create dic key=parent value=parent's size
        d_parent_size={}
        for i in a_parent:
            d_parent_size[i] = cv2.contourArea(contours[i])
        
        #criteria for soldier license plate
        min_no_children=2
        min_parent_size=5000

        has_soldier_plate=0
        for i in a_parent:
            cri=[0,0,0]
            if len(d_children[i])>=min_no_children:
                cri[0]=1
            if d_color_children[i]==1:
                cri[1]=1
            if d_parent_size[i]>=min_parent_size:
                cri[2]=1
            if sum(cri)==len(cri):
                cv2.drawContours(img6,[contours[i]],0,(0,255,0),1)
                children=d_children[i]
                for j in children:
                    cv2.drawContours(img6,[contours[j]],0,(0,0,255),1)
                    img_ori[h1:h0,w1:w2]=img6
                has_soldier_plate=1
        if has_soldier_plate==1:
            output_path=os.path.join(output_folder_path,file_name)
            cv2.imwrite(output_path,img_ori)
   
    count=count+1
    print str(count) +'/'+str(input_len)

