import cv2
import numpy as np
import os
import shutil

def f_pic_compare(pic_a,pic_b):
    my_sum=0
    my_size=pic_a.size
    x,y=pic_a.shape
    for i in xrange(x):
        for j in xrange(y):
            if pic_a[i,j]==pic_b[i,j]:
                my_sum=my_sum+1
    if my_sum==my_size:
        return 1
    else:
        return 0

def f_detect_letter_area(area_max,area_min,order_number_of_contour,all_contours):
    area=cv2.contourArea(all_contours[order_number_of_contour])
    if area > area_max or area < area_min:
        return 0
    else:
        return 1



def f_detect_letter_color(im_crop_bi,order_number_of_contour,all_contours):
    #output = 1 -> white letter 
    #output = 2 -> black letter
    #output = 3 -> something wrong
    
    cnt=all_contours[order_number_of_contour]
    leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
    rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
    topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
    
    v1= im_crop_bi[topmost[1]+1,topmost[0]]
    v2= im_crop_bi[bottommost[1]-1,bottommost[0]]
    v3= im_crop_bi[leftmost[1],leftmost[0]+1]
    v4= im_crop_bi[rightmost[1],rightmost[0]-1]


    if v1==v2==v3==v4==255:
        return 1
    elif v1==v1==v3==v4==0:
        return 2
    else:
        return 3
def f_detect_letter_bottommost(order_number_of_contour,all_contours):
    cnt=all_contours[order_number_of_contour]
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
    return bottommost

def f_most(d_most,max_dif_pos):
    a_order_contour=[]
    a_most=[]
    for key,value in sorted(d_most.items(),key=lambda xxx:xxx[1]):
        a_order_contour.append(key)
        a_most.append(value)
    
    #group most which near each other
    a_most_group=[]
    order_group=0
    for i in xrange(len(a_most)):
        if i==0:
            a_most_group.append([a_most[i]])
        else:
            dif=a_most[i]-a_most[i-1]
            if dif < max_dif_pos :
                a_most_group[order_group].append(a_most[i])
            else:
                a_most_group.append([a_most[i]])
                order_group=order_group+1
    
    #find group most which has the most member
    number_of_group = order_group + 1
    
    len_most_group=[]
    for i in a_most_group :
        len_most_group.append(len(i))
    max_len_most_group=max(len_most_group)
    
    index_max_len_most_group=len_most_group.index(max_len_most_group)
    num_loop=index_max_len_most_group+1
   
    
    
    stop_index_max_len_most=0
    for i in xrange(num_loop):
        start_index_max_len_most=stop_index_max_len_most
        stop_index_max_len_most= start_index_max_len_most + len_most_group[i]
    index_max_most=a_order_contour[start_index_max_len_most:stop_index_max_len_most]
    return index_max_most

################################################################################################
##This script is suitable for image size 768x576 only
cwd = os.getcwd()
input_folder_path=os.path.join(cwd,'input')
output_folder_path=os.path.join(cwd,'output')
if os.path.exists(output_folder_path):
    shutil.rmtree(output_folder_path)
os.makedirs(output_folder_path)
files =os.listdir(input_folder_path)
count=0
input_len=len(files)
area_max=700
area_min=90
max_dif_pos_y=20
max_dif_pos_x=100
min_number_letter=3
for file_name in files:
    if file_name[-11:]=='process.jpg':
        continue
    #grayscale
    im_full_color=cv2.imread(file_name,1)
    im_full_gray =cv2.imread(file_name,0)
    h0,w0 = im_full_gray.shape

    #crop only 1/2 of image below & 3/5 of middle
    h1=h0*1/2
    w1=w0/5
    w2=w0-w1
    im_crop_gray=im_full_gray[h1:h0,w1:w2]
    ##cv2.imshow('crop',img1)

    #im_crop_color and mask_crop to draw contour
    im_crop_color = cv2.cvtColor(im_crop_gray,cv2.COLOR_GRAY2RGB)
    mask_crop = np.zeros(im_crop_gray.shape,np.uint8)

    #median filter
    im_crop_median = cv2.medianBlur(im_crop_gray,3)
    ##cv2.imshow('medianblur',img1x)

    #blur
    im_crop_blur = cv2.bilateralFilter(im_crop_median,15,35,35)
    ##im_crop_blur = cv2.bilateralFilter(im_crop_median,15,5,5)
    ##cv2.imshow('bifil',img2)

    #adaptive binary
    im_crop_bi = cv2.adaptiveThreshold(im_crop_blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,63,2)
    ##cv2.imshow('im_crop_bi',im_crop_bi)

    #find contour
    img, contours_bi, hierarchy_bi = cv2.findContours(im_crop_bi,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    #find white letter
    a_candidate_letter=[]
    for i in xrange(len(contours_bi)):
        temp=f_detect_letter_area(area_max,area_min,i,contours_bi)
        if temp == 0:
            continue
        temp=f_detect_letter_color(im_crop_bi,i,contours_bi)
        if temp == 1:
            a_candidate_letter.append(i)
           
    #create dictionary key=order of contour value = bottommost
    d_bottommost_x={}
    d_bottommost_y={}
    d_bottommost_x_final={}
    for i in a_candidate_letter:
        bottommost=f_detect_letter_bottommost(i,contours_bi)
        d_bottommost_x[i]=bottommost[0]
        d_bottommost_y[i]=bottommost[1]

    
    index_max_most_y=f_most(d_bottommost_y,max_dif_pos_y)
    for i in index_max_most_y:
        d_bottommost_x_final[i]=d_bottommost_x[i]

    index_max_most_x=f_most(d_bottommost_x_final,max_dif_pos_x)
    if len(index_max_most_x)<min_number_letter:
        continue
    else:
        output_path=os.path.join(output_folder_path,file_name)
        im_crop_color=cv2.cvtColor(im_crop_bi,cv2.COLOR_GRAY2RGB)
        cv2.imwrite(output_path,img_ori)

count=count+1
print str(count) +'/'+str(input_len)
                              
        ##################################################################
        
        im_crop_color=cv2.cvtColor(im_crop_bi,cv2.COLOR_GRAY2RGB)
        for i in index_max_most_x:
        ##    im_crop_color=cv2.cvtColor(im_crop_bi,cv2.COLOR_GRAY2RGB)
            cv2.drawContours(im_crop_color,[contours_bi[i]],0,(0,255,0),1)
        cv2.imshow('final',im_crop_color)           
 
  








