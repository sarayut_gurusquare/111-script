import cv2
import numpy as np
import os
import sys


def f_have_license(pic_path):
    if os.path.exists(pic_path):
        has_license=0
        #grayscale
        ##mypath = os.getcwd()
        ##pic_path = os.path.join(mypath,'car1.jpg')
        img1=cv2.imread(pic_path,0)
        #pic to draw contour
        img6 = cv2.cvtColor(img1,cv2.COLOR_GRAY2RGB)
        ##median filter
        img1 = cv2.medianBlur(img1,5)
        #blur
        img2 = cv2.bilateralFilter(img1,9,75,75)
        #binary
        ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
        #find contour
        img5, contours, hierarchy = cv2.findContours(img3,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
        if len(contours)>0:
            #pick contours which is parent to other contour
            parent_contours=[]
            for i in range(0,len(hierarchy[0])):
                if hierarchy[0,i,2] != -1:
                    parent_contours.append(contours[i])
            #contour to convex hull
            hull_contours = parent_contours
            for i in range(0,len(parent_contours)):
                hull_contours[i] = cv2.convexHull(parent_contours[i])
            #filter by bounding box's properties
            contours=[]
            wh_r_max = 5
            wh_r_min = 2
            area_r_max = 14000
            area_r_min = 5000
            degree_max = 2.5
            degree_min = -2.5
            area_ratio_min = 0.8
            for i in range(0,len(hull_contours)):
                is_license_plate = 0
                temp = [0,0,0,0]
                cnt = hull_contours[i]
                #rotated bounding box
                rect = cv2.minAreaRect(cnt)
                wr = rect[1][0]
                hr = rect[1][1]
                area_r = wr*hr
                wh_r = wr/hr
                degree = rect[2]
                #straigt bounding box
                x,y,ws,hs = cv2.boundingRect(cnt)
                area_s = ws*hs
                area_ratio = area_r / area_s
                if wh_r_min < wh_r < wh_r_max:
                    temp[0]=1
                if area_r_min < area_r < area_r_max :
                    temp[1]=1
                if degree_min < degree < degree_max:
                    temp[2]=1
                if area_ratio_min < area_ratio:
                    temp[3]=1
                if sum(temp)==4:
                    is_license_plate = 1
                if is_license_plate == 1:
                    has_license = 1
##                    box = cv2.boxPoints(rect)
##                    box = np.int0(box)
##                    cv2.drawContours(img6,[box],0,(0,255,0),2)
##        mypath = os.getcwd()
        #cv2.imwrite(os.path.join(mypath,'output_'+str(has_license)+'_'+os.path.basename(pic_path)),img6)
        return str(has_license)
    
pic_path=sys.argv[1]
ans=f_have_license(pic_path)
print ans

   

















