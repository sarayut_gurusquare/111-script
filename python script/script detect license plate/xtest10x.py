import cv2
import numpy as np
import os
import shutil

def f_pic_compare(pic_a,pic_b):
    my_sum=0
    my_size=pic_a.size
    x,y=pic_a.shape
    for i in xrange(x):
        for j in xrange(y):
            if pic_a[i,j]==pic_b[i,j]:
                my_sum=my_sum+1
    if my_sum==my_size:
        return 1
    else:
        return 0


def f_detect_letter(order_number_of_contour,all_contours,all_hierachies,image_binary):
    #function to detect letter in binary image
    #output = 1 -> white letter without hole
    #output = 2 -> black letter without hole
    #output = 3 -> white letter with hole 
    #output = 4 -> black letter with hole
    im_self=np.zeros(image_binary.shape,np.uint8)
    cv2.drawContours(im_self,[all_contours[order_number_of_contour]],0,255,-1)
    im_x=cv2.bitwise_and(image_binary,image_binary,mask=im_self)
    if f_pic_compare(im_self,im_x)==1:
        return 1
    else:
        img, contours_x, hierarchy_x = cv2.findContours(im_x,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        im_x_child=np.zeros(image_binary.shape,np.uint8)
        cv2.drawContours(im_x_child,[contours_x[1]],0,255,-1)    
        if f_pic_compare(im_x_child,im_self)==0:
            return 3
        else:
            h=hierarchy_x[0]
            if len(h)==2:
                return 2
            else:
                return 4

################################################################################################
file_name='b4.png'
#grayscale
im_gray=cv2.imread(file_name,0)

#adaptive binary
ret,im_bi = cv2.threshold(im_gray,127,255,cv2.THRESH_BINARY)
im_bw=np.zeros(im_bi.shape,np.uint8)
##cv2.imshow('im_bi',im_bi)

imgm, contours_bi, hierarchy_bi = cv2.findContours(im_bi,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
order_number_of_contour=1
print f_detect_letter(order_number_of_contour,contours_bi,hierarchy_bi,im_bi)
##    cv2.waitKey(0)
##    cv2.destroyAllWindows()





