import cv2
import numpy as np
from matplotlib import pyplot as plt

#grayscale
img1 = cv2.imread('soldier.jpg',0)
##cv2.imshow('gray',img1)

#pic to draw contour
img6 = cv2.cvtColor(img1,cv2.COLOR_GRAY2RGB)

##median filter
img1 = cv2.medianBlur(img1,5)

#blur
img2 = cv2.bilateralFilter(img1,9,75,75)
##img2 = cv2.GaussianBlur(img1,(7,7),0)
##img2 = cv2.blur(img1,(5,5))
cv2.imshow('blur',img2)

#binary
ret,img3 = cv2.threshold(img2,127,255,cv2.THRESH_BINARY)
cv2.imshow('binary',img3)

#find contour
img5, contours, hierarchy = cv2.findContours(img3,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
cv2.imshow('contour',img5)
#pick contours which is parent to other contour
parent_contours=[]
for i in range(0,len(hierarchy[0])):
    if hierarchy[0,i,2] != -1:
        parent_contours.append(contours[i])

#contour to convex hull
hull_contours = parent_contours
for i in range(0,len(parent_contours)):
    hull_contours[i] = cv2.convexHull(parent_contours[i])

#draw convex hull
cv2.drawContours(img6, hull_contours, -1, (255,0,255), 2)

#draw bounding box
contours=[]
for i in range(0,len(hull_contours)):
    cnt = hull_contours[i]
    rect = cv2.minAreaRect(cnt)
    w = rect[1][0]
    h = rect[1][1]
    area = w*h
    area_max = 14000
    area_min = 5000
    if area_min < area < area_max :
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(img6,[box],0,(0,255,0),2)
cv2.imshow('rectangle',img6)




################################################################################################################################
cv2.waitKey(0)
cv2.destroyAllWindows()
















