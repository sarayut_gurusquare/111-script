#sample 1
dic={'a':3,'b':1,'c':2}
d={}
ax=[]
ay=[]
#a:a[1] means to sort by the second element(a[1]) and show result by all element (a) 
for x,y in sorted(dic.items(),key=lambda a:a[1]):
    ax.append(x)
    ay.append(y)


#sample 2
lis=[14,11,13,12]
#b:a[b] means to sort by lis[k] (value) and show result by k (index)
ans1=sorted(range(len(lis)),key=lambda k: lis[k])

#sample 3 return value
lis=[14,11,13,12]
ans2 = sorted(lis)
