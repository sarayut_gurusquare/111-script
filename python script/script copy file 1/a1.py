#script move items in input folder to output folder
#structure must be like this
#input folder
#   folder 1
#       item 1
#       item 2
#   folder 2
#       item 3
#       item 4
#   folder n
#       item x
#       item y
#output folder
#   item 1
#   item 2
#   item 3
#   item 4
#   item x
#   item y

import os, shutil
cwd = os.getcwd()
input_dir_path  = os.path.join(cwd,'input')
output_dir_path = os.path.join(cwd,'output')
subfols = os.listdir(input_dir_path)
for i in xrange(len(subfols)):
    subfol = subfols[i]
    subfol_path = os.path.join(cwd,'input',subfol)
    items = os.listdir(subfol_path)
    for j in xrange(len(items)):
        src_path = os.path.join(subfol_path,items[j])
        des_path = os.path.join(output_dir_path,items[j])
        shutil.copy(src_path,des_path)
        
    
    

