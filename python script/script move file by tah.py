import shutil
import os
from multiprocessing.pool import ThreadPool

def move(a, b):
    shutil.move(a, b)


src = 'c:/guru-lpr/services/ftp-sync/data/lpr/out'
target = 'c:/guru-lpr/storage/ftp-sync/lpr/pending'

assert os.path.exists(src)
assert os.path.exists(target)

def renamer(filename):
    tokens = filename.split('_')
    dir, ip = tokens[:2]
    filename = tokens[2:]
    assert len(filename) > 0
    return '_'.join(filename)

def do_file(filename):
    src_path = os.path.join(src, filename)
    target_path = os.path.join(target, renamer(filename))
    move(src_path, target_path)

files = os.listdir(src)
pool = ThreadPool(10)
for i, (filename, _) in enumerate(zip(files, pool.imap(do_file, files)), 1):
    print('({}/{}) file: {}'.format(i, len(files), filename))

pool.close()
pool.join()

print('done!')
