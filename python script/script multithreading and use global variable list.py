import multiprocessing, time

def my_task(args):
    # IMPORTANT : print anything in this function will be invisible because of map_async
    count = args[0]
    temp_list = args[1]
    temp_list.append(count)
    return 'done'
    
def main():
    pool = multiprocessing.Pool(5)
    my_result = pool.map_async(my_task, [(x, my_list) for x in range(10)])
    # IMPORTANT : If print element in my_list now, it probably not show anythings because
    # map_async doesn't wait till itself finish and continue to run next line
    # which in that time, not yet finish even 1 task from 10 tasks.
    # To be able to print elements in my_list, there are 2 solutions 
    # method1 : my_result.get() will wait till it finish ALL tasks and return result
    # method2 : wait for some time till the tasks finish
########################################
# method1 : 
    my_result.get()
    for i in my_list:
        print i    
########################################
# method2 :
#    time.sleep(1)
#    for i in my_list:
#        print i
########################################
if __name__ == '__main__':
    #declare my_list here to be global variable (declare outside all functions)
    manager = multiprocessing.Manager()
    my_list = manager.list()
    main()
