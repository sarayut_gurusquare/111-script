"""
change big to small pic
"""

import os, cv2

cwd                 = os.getcwd()
path_input_folder   = os.path.join(cwd,'input')
path_output_folder  = os.path.join(cwd,'output')

files               = os.listdir(path_input_folder)
for file in files :
    print file
    im1             = cv2.imread(os.path.join(path_input_folder,file),1)
    im2             = cv2.resize(im1,None,fx=0.2, fy=0.2, interpolation = cv2.INTER_AREA )
    cv2.imwrite(os.path.join(path_output_folder,file),im2)
    

    
    
cv2.waitKey(0)
cv2.destroyAllWindows()
