#when use os.getcwd() in command line, it show different path as it used to be 
(it show current path of command line not current path of called python file)
#the solution is changing as below code   

import os
cwd = os.path.dirname(os.path.realpath(__file__))