import os, sys, shutil,time
# how to use -> use in command line
# python.exe a1.py mod_num pics_input_folder_path pics_output_folder_path
# example -> python.exe a1.py 500 "D:\abc\in" "D:\abc\out"
###################
#mod_num                     = 50
#pics_input_folder_path      = r"D:\abc\in"
#pics_output_folder_path     = r"D:\abc\out"
###################

if len(sys.argv)==4:
    mod_num                     = int(sys.argv[1])
    pics_input_folder_path      = sys.argv[2]
    pics_output_folder_path     = sys.argv[3]
    temp_folder_path            = r"D:\abc\temp"
    if os.path.exists(pics_input_folder_path):
        if os.path.exists(pics_output_folder_path):
            shutil.rmtree(pics_output_folder_path)
        os.mkdir(pics_output_folder_path)
        if os.path.exists(temp_folder_path):
            shutil.rmtree(temp_folder_path)
        os.mkdir(temp_folder_path)
        in_list                     = os.listdir(pics_input_folder_path)
        for i in xrange(len(in_list)):
            if i % mod_num == 0:
                shutil.copy(os.path.join(pics_input_folder_path,in_list[i]),os.path.join(temp_folder_path,in_list[i]))
                shutil.move(os.path.join(temp_folder_path,in_list[i]),os.path.join(pics_output_folder_path,in_list[i]))
                time.sleep(0.125)
                print str(i)+'/'+str(len(in_list))
    
        shutil.rmtree(temp_folder_path)
    else:
        print 'folder input not exist!!!'
        