import numpy as np

# add column to numpy array
a1=np.array([[1, 2, 3]])
a1.shape=(3,1)

a2=np.array([[4,5],[6,7],[8,9]])
a3=np.append(a2,a1,axis=1)


# change list to numpy array
a4=[]
a4.append([5,6])
a4.append([7,8])
npa = np.asarray(a4, dtype=np.float32)
