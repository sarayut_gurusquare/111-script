import subprocess, os

cwd                     = os.getcwd()
path_tesseract          = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"
path_image              = os.path.join(cwd, 'a1.jpg')
path_text               = os.path.join(cwd, 'result')


subprocess.call([path_tesseract,path_image,path_text])
