import subprocess, os, time
########################################################################
#####delete old output
for i in range(5):
    temp_path = 'test'+str(i+1)+'.txt'
    if os.path.exists(temp_path)==1:
        os.remove(temp_path)
########################################################################
cmd = ['pythonw.exe', 'becalled.py']
p = subprocess.Popen(cmd,stdout=subprocess.PIPE)

########################################################################
#####IMPORTANT!!! 
#####If you want to call and wait becalled.py to run till it finish its job then continue run next line, 
#####you need to uncomment codes below.If p.returncode is 0, then the job has been finished completely.
#####If you just want to call becalled.py and run the next line of callme.py without waiting becalled.py 
#####to finish its job, you don't need to uncomment codes below


#p.wait()
#print p.returncode
########################################################################
while True:
    time.sleep(1)
print 'call finish'

